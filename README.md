# VKU - Vulkan Utilities

Simple [STB-style](https://github.com/nothings/stb/blob/master/docs/stb_howto.txt) header file with Vulkan utility functions.

All code is written in plain C but should work with most modern C++ compilers as well.
