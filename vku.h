/*	vku.h - v0.02 - vulkan utilities
	public domain; no warranty implied; use at your own risk

	http://micha.monoid.net/vku/


USAGE
	Do this:
		#define VKU_IMPLEMENTATION
	before you include this file in *1* C or C++ file for the definitions.

	Optionally provide the following defines yourself:
		VKU_API_DECL - public function declaration prefix (def: extern)
		VKU_API_IMPL - public function definition prefix (def: -)
		VKU_DLL - if exported/imported as a DLL (win32 only)
		VKU_INCLUDE_DEPS - if defined, includes dependency headers


NOTES
	The code is primarily written for C99 and not ISO C++ conformant.
	Most modern C++ compilers handle _compound literals_ just fine though.


LICENSE
	See end of file for license information.


RECENT REVISION HISTORY
	0.02 simplified macros and formatting (2023-01-15)
	0.01 first released version (2022-09-17)

	See end of file for full revision history.
*/
#ifndef VKU_H
#define VKU_H 1

///////////////////////////////////////////////////////////////////////////////
// META

// Dependencies to include for IDEs (errors, completion, ..).
// External guards for performance and explicit exclusion.
#ifdef VKU_INCLUDE_DEPS
#	include <stdio.h>
#	include <string.h>
#	include <assert.h>
#	include <vulkan/vulkan.h>
#endif

// API declaration prefix
#ifndef VKU_API_DECL
#	if defined(_WIN32) && defined(VKU_DLL) && defined(VKU_API_IMPL)
#		define VKU_API_DECL __declspec(dllexport) extern
#	elif defined(_WIN32) && defined(VKU_DLL)
#		define VKU_API_DECL __declspec(dllimport)
#	else
#		define VKU_API_DECL extern
#	endif
#endif

// API implementation prefix (default: -)
#ifndef VKU_API_IMPL
#	define VKU_API_IMPL
#endif

// C++ support
#ifdef __cplusplus
extern "C" {
#endif

///////////////////////////////////////////////////////////////////////////////
// API

// Clamp the width and height to be within a min/max range.
VKU_API_DECL VkExtent2D
vkuClampExtent2D(VkExtent2D extent,
                 VkExtent2D min,
                 VkExtent2D max);

// Clamp the width, height and depth to be within a min/max range.
VKU_API_DECL VkExtent3D
vkuClampExtent3D(VkExtent3D extent,
                 VkExtent3D min,
                 VkExtent3D max);


// Advance the offset to match the alignment if necessary.
VKU_API_DECL VkDeviceSize
vkuAlignOffset(VkDeviceSize offset,
               VkDeviceSize alignment);


// Find the first memory type with matching requirements and type bit.
// If the result is equal to the memory type count, no entry valid was found.
VKU_API_DECL uint32_t
vkuFindMemoryTypeIndex(VkMemoryPropertyFlags propertyRequirements,
                       uint32_t memoryTypeBits,
                       uint32_t memoryTypeCount,
                       const VkMemoryType pMemoryTypes[/*memoryTypeCount*/]);


// Find the first layer properties with a matching name.
// If the result is equal to the property count, no entry valid was found.
VKU_API_DECL uint32_t
vkuFindLayerIndex(const char* pName,
                  uint32_t count,
                  const VkLayerProperties pProperties[/*count*/]);

// Find the first extension properties with a matching name.
// If the result is equal to the property count, no entry valid was found.
VKU_API_DECL uint32_t
vkuFindExtensionIndex(const char* pName,
                      uint32_t count,
                      const VkExtensionProperties pProperties[/*count*/]);

// Partition the layer names into those available (matching name) and not.
// Returns the size of the available names.
VKU_API_DECL uint32_t
vkuPartitionLayers(uint32_t requested,
                   const char *pNames[/*requested*/],
                   uint32_t available,
                   const VkLayerProperties pProperties[/*available*/]);

// Partition the extension names into those available (matching name) and not.
// Returns the size of the available names.
VKU_API_DECL uint32_t
vkuPartitionExtensions(uint32_t requested,
                       const char *pNames[/*requested*/],
                       uint32_t available,
                       const VkExtensionProperties pProperties[/*available*/]);


// Initialize the image as write target in the desired layout.
// All mip levels and layers as color storage (`VK_IMAGE_ASPECT_COLOR_BIT`).
// The image original layout must be `VK_IMAGE_LAYOUT_UNDEFINED`.
VKU_API_DECL void
vkuInitColorImage2D(VkCommandBuffer commandBuffer,
                    VkImage image,
                    VkImageLayout layout);

// Copiy the base mip level from the buffer to the image.
// The image layout must be `VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL`.
// The image usage flags must include `VK_IMAGE_USAGE_TRANSFER_DST_BIT`.
VKU_API_DECL void
vkuUploadColorImage2DBase(VkCommandBuffer commandBuffer,
                          VkImage image,
                          VkExtent2D extent,
                          VkBuffer srcBuffer,
                          VkDeviceSize srcOffset);

// Generate the mip-levels and transfer all into the desired layout.
// The image original layout must be `VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL`.
// The image usage flags must include `VK_IMAGE_USAGE_TRANSFER_DST_BIT`.
VKU_API_DECL void
vkuGenerateColorImage2DMips(VkCommandBuffer commandBuffer,
                            VkExtent2D extent,
                            uint32_t mipLevels,
                            VkImage image,
                            VkImageLayout layout);


// Name of the result from name vulkan functions.
VKU_API_DECL const char*
vkuNameResult(VkResult result);


// Name the VK_EXT_debug_utils message flags.
VKU_API_DECL const char*
vkuNameDebugUtilsMessageSeverityFlagsEXT(VkDebugUtilsMessageSeverityFlagsEXT flags);

// Name the VK_EXT_debug_utils message types.
VKU_API_DECL const char*
vkuNameVkDebugUtilsMessageTypeFlagsEXT(VkDebugUtilsMessageTypeFlagsEXT flags);

// Callback function for VK_EXT_debug_utils messages.
// Expects the user data to be a valid FILE*.
VKU_API_DECL VkBool32
vkuDebugUtilsMessengerCallbackEXT(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                                  VkDebugUtilsMessageTypeFlagsEXT messageType,
                                  const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
                                  void* pUserData);


// Query instance-level version before instance creation
// Same as vkEnumerateInstanceVersion with fallback for VK_API_VERSION_1_0.
VKU_API_DECL VkResult
vkuEnumerateInstanceVersion(uint32_t* pApiVersion);

///////////////////////////////////////////////////////////////////////////////
// IMPLEMENTATION

#ifdef VKU_IMPLEMENTATION

// vkuClampExtent2D/3D

#ifdef _vkuMin
#	error "_vkuMin already defined."
#endif

#ifdef _vkuMax
#	error "_vkuMax already defined."
#endif

#ifdef _vkuClamp
#	error "_vkuClamp already defined."
#endif

#define _vkuMin(value,min) ((value) < (min) ? (min) : (value))
#define _vkuMax(value,max) ((value) > (max) ? (max) : (value))
#define _vkuClamp(value,min,max) _vkuMax(_vkuMin(value, min), max)

VKU_API_IMPL VkExtent2D
vkuClampExtent2D(VkExtent2D extent,
                 VkExtent2D min,
                 VkExtent2D max) {
	return (VkExtent2D) {
		.width = _vkuClamp(extent.width, min.width, max.width),
		.height = _vkuClamp(extent.height, min.height, max.height),
	};
}

VKU_API_IMPL VkExtent3D
vkuClampExtent3D(VkExtent3D extent,
                 VkExtent3D min,
                 VkExtent3D max) {
	return (VkExtent3D) {
		.width = _vkuClamp(extent.width, min.width, max.width),
		.height = _vkuClamp(extent.height, min.height, max.height),
		.depth = _vkuClamp(extent.depth, min.depth, max.depth),
	};
}

#undef _vkuClamp
#undef _vkuMax
#undef _vkuMin

// vkuAlignOffset

VKU_API_IMPL VkDeviceSize
vkuAlignOffset(VkDeviceSize offset,
               VkDeviceSize alignment) {
	// https://en.wikipedia.org/wiki/Data_structure_alignment
	return (offset + (alignment - 1)) & -alignment;
}

// vkuFind[MemoryType|Layer|Extension]Index

VKU_API_IMPL uint32_t
vkuFindMemoryTypeIndex(VkMemoryPropertyFlags propertyRequirements,
                       uint32_t memoryTypeBits,
                       uint32_t memoryTypeCount,
                       const VkMemoryType pMemoryTypes[/*memoryTypeCount*/]) {
	uint32_t memoryTypeIndex = memoryTypeCount;
	for (uint32_t i = 0; i < memoryTypeCount; i++) {
		VkBool32 matchingBits = 1 & (memoryTypeBits >> i);
		VkBool32 matchingFlags = (pMemoryTypes[i].propertyFlags & propertyRequirements) == propertyRequirements;
		if (matchingBits & matchingFlags) {
			memoryTypeIndex = i;
			break;
		}
	}
	return memoryTypeIndex;
}

VKU_API_IMPL uint32_t
vkuFindLayerIndex(const char* pName,
                  uint32_t count,
                  const VkLayerProperties pProperties[/*count*/]) {
	for (uint32_t i = 0; i < count; i++) {
		if (strcmp(pName, pProperties[i].layerName) == 0) {
			return i;
		}
	}
	return count;
}

VKU_API_IMPL uint32_t
vkuFindExtensionIndex(const char* pName,
                      uint32_t count,
                      const VkExtensionProperties pProperties[/*count*/]) {
	for (uint32_t i = 0; i < count; i++) {
		if (strcmp(pName, pProperties[i].extensionName) == 0) {
			return i;
		}
	}
	return count;
}

// vkuPartition[Layer|Extension]s

VKU_API_IMPL uint32_t
vkuPartitionLayers(uint32_t requested,
                   const char *names[/*requested*/],
                   uint32_t available,
                   const VkLayerProperties properties[/*available*/]) {
	for (uint32_t i = 0; i < requested; i++) {
		if (vkuFindLayerIndex(names[i], available, properties) < available) {
			continue;
		}
		requested--;
		// swap with last
		const char *tmp = names[requested];
		names[requested] = names[i];
		names[i] = tmp;
		i--;
	}
	return requested;
}

VKU_API_IMPL uint32_t
vkuPartitionExtensions(uint32_t requested,
                       const char *names[/*requested*/],
                       uint32_t available,
                       const VkExtensionProperties properties[/*available*/]) {
  	for (uint32_t i = 0; i < requested; i++) {
    		if (vkuFindExtensionIndex(names[i], available, properties) < available) {
      			continue;
		}
		requested--;
		// swap with last
		const char *tmp = names[requested];
		names[requested] = names[i];
		names[i] = tmp;
		i--;
	}
	return requested;
}


// image utils

VKU_API_IMPL void
vkuInitColorImage2D(VkCommandBuffer commandBuffer,
                    VkImage image,
                    VkImageLayout layout) {
	VkImageMemoryBarrier imageMemoryBarrier = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
		.pNext = NULL,
		.srcAccessMask = 0,
		.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
		.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		.newLayout = layout,
		.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
		.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
		.image = image,
		.subresourceRange = {
			.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
			.baseMipLevel = 0,
			.levelCount = VK_REMAINING_MIP_LEVELS,
			.baseArrayLayer = 0,
			.layerCount = VK_REMAINING_ARRAY_LAYERS,
		},
	};
	vkCmdPipelineBarrier(
		commandBuffer,
		VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
		VK_PIPELINE_STAGE_TRANSFER_BIT,
		0,
		0, NULL,
		0, NULL,
		1, &imageMemoryBarrier);
}

VKU_API_IMPL void
vkuUploadColorImage2DBase(VkCommandBuffer commandBuffer,
                          VkImage image,
                          VkExtent2D extent,
                          VkBuffer srcBuffer,
                          VkDeviceSize srcOffset) {
	VkBufferImageCopy region = {
		.bufferOffset = srcOffset,
		.bufferRowLength = 0,
		.bufferImageHeight = 0,
		.imageSubresource = {
			.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
			.mipLevel = 0,
			.baseArrayLayer = 0,
			.layerCount = 1,
		},
		.imageOffset = { 0, 0, 0},
		.imageExtent = {
			.width = extent.width,
			.height = extent.height,
			.depth = 1,
		},
	};
	vkCmdCopyBufferToImage(
		commandBuffer,
		srcBuffer,
		image,
		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		1, &region);
}

VKU_API_IMPL void
vkuGenerateColorImage2DMips(VkCommandBuffer commandBuffer,
                            VkExtent2D extent,
                            uint32_t mipLevels,
                            VkImage image,
                            VkImageLayout layout) {
	VkImageMemoryBarrier imageMemoryBarrier;
	for (uint32_t mipLevel = 1; mipLevel < mipLevels; mipLevel++) {
		VkExtent2D next_extent = {
			.width = (extent.width > 1) ? extent.width / 2 : 1,
			.height = (extent.height > 1) ? extent.height / 2 : 1,
		};

		// wait until previous mip-level can be read
		imageMemoryBarrier = (VkImageMemoryBarrier) {
			.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
			.pNext = NULL,
			.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
			.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
			.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
			.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
			.image = image,
			.subresourceRange = {
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = mipLevel - 1,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
		};
		vkCmdPipelineBarrier(
			commandBuffer,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			0,
			0, NULL,
			0, NULL,
			1, &imageMemoryBarrier);

		// generate next mip level from previous
		VkImageBlit region = {
			.srcSubresource = {
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.mipLevel = mipLevel - 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
			.srcOffsets = {
				{ 0, 0, 0 },
				{ (int32_t) extent.width, (int32_t) extent.height, 1 },
			},
			.dstSubresource = {
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.mipLevel = mipLevel,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
			.dstOffsets = {
				{ 0, 0, 0 },
				{ (int32_t) next_extent.width, (int32_t) next_extent.height, 1 },
			},
		};
		vkCmdBlitImage(
			commandBuffer,
			image,
			VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			1, &region,
			VK_FILTER_LINEAR);

		// wait until previous mip-level can be used in a shader
		imageMemoryBarrier = (VkImageMemoryBarrier) {
			.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
			.pNext = NULL,
			.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
			.dstAccessMask = 0, // VK_ACCESS_TRANSFER_READ_BIT,
			.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			.newLayout = layout,
			.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
			.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
			.image = image,
			.subresourceRange = {
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = mipLevel - 1,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
		};
		vkCmdPipelineBarrier(
			commandBuffer,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, // VK_PIPELINE_STAGE_TRANSFER_BIT,
			0,
			0, NULL,
			0, NULL,
			1, &imageMemoryBarrier);

		extent = next_extent;
	}

	// wait until last mip-level can be used in a shader
	imageMemoryBarrier = (VkImageMemoryBarrier) {
		.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
		.pNext = NULL,
		.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
		.dstAccessMask = 0, // VK_ACCESS_TRANSFER_READ_BIT,
		.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		.newLayout = layout,
		.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
		.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
		.image = image,
		.subresourceRange = {
			.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
			.baseMipLevel = mipLevels - 1,
			.levelCount = 1,
			.baseArrayLayer = 0,
			.layerCount = 1,
		},
	};
	vkCmdPipelineBarrier(
		commandBuffer,
		VK_PIPELINE_STAGE_TRANSFER_BIT,
		VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, // VK_PIPELINE_STAGE_TRANSFER_BIT,
		0,
		0, NULL,
		0, NULL,
		1, &imageMemoryBarrier);
}

// vkuNameResult (according to Version 1.3.228)

VKU_API_IMPL const char*
vkuNameResult(VkResult result) {
	switch (result) {
	case VK_SUCCESS: return "VK_SUCCESS";
	case VK_NOT_READY: return "VK_NOT_READY";
	case VK_TIMEOUT: return "VK_TIMEOUT";
	case VK_EVENT_SET: return "VK_EVENT_SET";
	case VK_EVENT_RESET: return "VK_EVENT_RESET";
	case VK_INCOMPLETE: return "VK_INCOMPLETE";
	case VK_ERROR_OUT_OF_HOST_MEMORY: return "VK_ERROR_OUT_OF_HOST_MEMORY";
	case VK_ERROR_OUT_OF_DEVICE_MEMORY: return "VK_ERROR_OUT_OF_DEVICE_MEMORY";
	case VK_ERROR_INITIALIZATION_FAILED: return "VK_ERROR_INITIALIZATION_FAILED";
	case VK_ERROR_DEVICE_LOST: return "VK_ERROR_DEVICE_LOST";
	case VK_ERROR_MEMORY_MAP_FAILED: return "VK_ERROR_MEMORY_MAP_FAILED";
	case VK_ERROR_LAYER_NOT_PRESENT: return "VK_ERROR_LAYER_NOT_PRESENT";
	case VK_ERROR_EXTENSION_NOT_PRESENT: return "VK_ERROR_EXTENSION_NOT_PRESENT";
	case VK_ERROR_FEATURE_NOT_PRESENT: return "VK_ERROR_FEATURE_NOT_PRESENT";
	case VK_ERROR_INCOMPATIBLE_DRIVER: return "VK_ERROR_INCOMPATIBLE_DRIVER";
	case VK_ERROR_TOO_MANY_OBJECTS: return "VK_ERROR_TOO_MANY_OBJECTS";
	case VK_ERROR_FORMAT_NOT_SUPPORTED: return "VK_ERROR_FORMAT_NOT_SUPPORTED";
	case VK_ERROR_FRAGMENTED_POOL: return "VK_ERROR_FRAGMENTED_POOL";
	case VK_ERROR_UNKNOWN: return "VK_ERROR_UNKNOWN";
#ifdef VK_API_VERSION_1_1
	case VK_ERROR_OUT_OF_POOL_MEMORY: return "VK_ERROR_OUT_OF_POOL_MEMORY";
	case VK_ERROR_INVALID_EXTERNAL_HANDLE: return "VK_ERROR_INVALID_EXTERNAL_HANDLE";
#else
#ifdef VK_KHR_maintenance1
 	case VK_ERROR_OUT_OF_POOL_MEMORY_KHR: return "VK_ERROR_OUT_OF_POOL_MEMORY_KHR";
#endif
#ifdef VK_KHR_external_memory
	case VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR: return "VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR";
#endif
#endif
#ifdef VK_API_VERSION_1_2
	case VK_ERROR_FRAGMENTATION: return "VK_ERROR_FRAGMENTATION";
	case VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS: return "VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS";
#else
#ifdef VK_EXT_descriptor_indexing
	case VK_ERROR_FRAGMENTATION: return "VK_ERROR_FRAGMENTATION";
#endif
#ifdef VK_KHR_buffer_device_address
	case VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS_KHR: return "VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS_KHR";
#else
#ifdef VK_EXT_buffer_device_address
	case VK_ERROR_INVALID_DEVICE_ADDRESS_EXT: return "VK_ERROR_INVALID_DEVICE_ADDRESS_EXT";
#endif
#endif
#endif
#ifdef VK_API_VERSION_1_3
	case VK_PIPELINE_COMPILE_REQUIRED: return "VK_PIPELINE_COMPILE_REQUIRED";
#else
#ifdef VK_API_VERSION_1_3
	case VK_PIPELINE_COMPILE_REQUIRED_EXT: return "VK_PIPELINE_COMPILE_REQUIRED_EXT";
#endif
#endif
#ifdef VK_KHR_surface
	case VK_ERROR_SURFACE_LOST_KHR: return "VK_ERROR_SURFACE_LOST_KHR";
	case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR: return "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR";
#endif
#ifdef VK_KHR_swapchain
	case VK_SUBOPTIMAL_KHR: return "VK_SUBOPTIMAL_KHR";
	case VK_ERROR_OUT_OF_DATE_KHR: return "VK_ERROR_OUT_OF_DATE_KHR";
#endif
#ifdef VK_KHR_display_swapchain
	case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR: return "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR";
#endif
#ifdef VK_EXT_debug_report
	case VK_ERROR_VALIDATION_FAILED_EXT: return "VK_ERROR_VALIDATION_FAILED_EXT";
#endif
#ifdef VK_NV_glsl_shader
	case VK_ERROR_INVALID_SHADER_NV: return "VK_ERROR_INVALID_SHADER_NV";
#endif
#ifdef VK_ENABLE_BETA_EXTENSIONS
#ifdef VK_KHR_video_queue
	case VK_ERROR_IMAGE_USAGE_NOT_SUPPORTED_KHR: return "VK_ERROR_IMAGE_USAGE_NOT_SUPPORTED_KHR";
	case VK_ERROR_VIDEO_PICTURE_LAYOUT_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PICTURE_LAYOUT_NOT_SUPPORTED_KHR";
	case VK_ERROR_VIDEO_PROFILE_OPERATION_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PROFILE_OPERATION_NOT_SUPPORTED_KHR";
	case VK_ERROR_VIDEO_PROFILE_FORMAT_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PROFILE_FORMAT_NOT_SUPPORTED_KHR";
	case VK_ERROR_VIDEO_PROFILE_CODEC_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PROFILE_CODEC_NOT_SUPPORTED_KHR";
	case VK_ERROR_VIDEO_STD_VERSION_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_STD_VERSION_NOT_SUPPORTED_KHR";
#endif
#endif
#ifdef VK_EXT_image_drm_format_modifier
	case VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT: return "VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT";
#endif
#ifdef VK_KHR_global_priority
	case VK_ERROR_NOT_PERMITTED_KHR: return "VK_ERROR_NOT_PERMITTED_KHR";
#else
#ifdef VK_KHR_global_priority
	case VK_ERROR_NOT_PERMITTED_EXT: return "VK_ERROR_NOT_PERMITTED_EXT";
#endif
#endif
#ifdef VK_EXT_full_screen_exclusive
	case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT: return "VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT";
#endif
#ifdef VK_KHR_deferred_host_operations
	case VK_THREAD_IDLE_KHR: return "VK_THREAD_IDLE_KHR";
	case VK_THREAD_DONE_KHR: return "VK_THREAD_DONE_KHR";
	case VK_OPERATION_DEFERRED_KHR: return "VK_OPERATION_DEFERRED_KHR";
	case VK_OPERATION_NOT_DEFERRED_KHR: return "VK_OPERATION_NOT_DEFERRED_KHR";
#endif
#ifdef VK_EXT_image_compression_control
	case VK_ERROR_COMPRESSION_EXHAUSTED_EXT: return "VK_ERROR_COMPRESSION_EXHAUSTED_EXT";
#endif
	default: return NULL;
	}
}

// debug utils

VKU_API_IMPL const char*
vkuNameDebugUtilsMessageSeverityFlagsEXT(VkDebugUtilsMessageSeverityFlagsEXT flags) {
	switch (flags) {
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
		return "Verbose | Info | Warning | Error";
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
		return "Info | Warning | Error";
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
		return "Verbose | Warning | Error";
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
		return "Verbose | Info | Error";
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
		return "Warning | Error";
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
		return "Info | Error";
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
		return "Verbose | Error";
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
		return "Error";
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
		return "Verbose | Info | Warning";
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
		return "Info | Warning";
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
		return "Verbose | Warning";
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
		return "Warning";
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
		return "Verbose | Info";
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
		return "Info";
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
		return "Verbose";
	default:
		return "Unknown";
    }
}

VKU_API_IMPL const char*
vkuNameVkDebugUtilsMessageTypeFlagsEXT(VkDebugUtilsMessageTypeFlagsEXT flags) {
	switch (flags) {
	case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
		return "General | Validation | Performance";
	case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
		return "Validation | Performance";
	case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
		return "General | Performance";
	case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
		return "Performance";
	case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT:
		return "General | Validation";
	case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT:
		return "Validation";
	case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT:
		return "General";
	default:
		return "Unknown";
	}
}

VKU_API_IMPL VkBool32
vkuDebugUtilsMessengerCallbackEXT(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                                  VkDebugUtilsMessageTypeFlagsEXT messageType,
                                  const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
                                  void* pUserData) {
#ifdef assert
	assert(pUserData != NULL);
#endif
	FILE *pFile = (FILE*) pUserData;

	const char* pSeverityText = vkuNameDebugUtilsMessageSeverityFlagsEXT(messageSeverity);
	const char* pTypeText = vkuNameVkDebugUtilsMessageTypeFlagsEXT(messageType);

	fprintf(pFile, "[%s: %s] %s\n", pSeverityText, pTypeText, pCallbackData->pMessage);
	return VK_FALSE;
}


VKU_API_IMPL VkResult
vkuEnumerateInstanceVersion(uint32_t* pApiVersion) {
#ifdef VK_API_VERSION_1_1
	return vkEnumerateInstanceVersion(pApiVersion);
#else
	*pApiVersion = VK_API_VERSION_1_0;
	return VK_SUCCESS;
#endif
}

#endif	// VKU_IMPLEMENTATION

///////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}	// extern "C"
#endif

#endif	// VKU_H

/*	REVISION HISTORY
-------------------------------------------------------------------------------
	0.02 simplified macros and formatting (2023-01-15)
	0.01 first released version (2022-09-17)
-------------------------------------------------------------------------------
*/

/*	LICENSE
-------------------------------------------------------------------------------
This software is available under 2 licenses -- choose whichever you prefer.
-------------------------------------------------------------------------------
ALTERNATIVE A - MIT License
Copyright (c) 2022 Michael Nischt (micha@monoid.net)
Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-------------------------------------------------------------------------------
ALTERNATIVE B - Public Domain (www.unlicense.org)
This is free and unencumbered software released into the public domain.
Anyone is free to copy, modify, publish, use, compile, sell, or distribute this
software, either in source code form or as a compiled binary, for any purpose,
commercial or non-commercial, and by any means.
In jurisdictions that recognize copyright laws, the author or authors of this
software dedicate any and all copyright interest in the software to the public
domain. We make this dedication for the benefit of the public at large and to
the detriment of our heirs and successors. We intend this dedication to be an
overt act of relinquishment in perpetuity of all present and future rights to
this software under copyright law.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-------------------------------------------------------------------------------
*/
